import math

class Node:
    def __init__(self, name, node_type, groups=None, init_rank=0, raw_rank=0, rank=None, created_at=None, verifications=None):
        self.name = name
        self.node_type = node_type
        self.rank = rank
        self.groups = groups if groups else {}
        self.init_rank = init_rank
        self.raw_rank = raw_rank
        self.created_at = created_at
        self.verifications = verifications if verifications else []

    def __repr__(self):
        return 'Node: {}'.format(self.name)


class SybilRank():
    def __init__(self, graph, options=None):
        self.graph = graph
        self.options = options if options else {}

    def rank(self):
        num_iterations = max(3, int(math.ceil(math.log10(self.graph.order()))))
        nodes_rank = {node: self.graph.nodes[node]['init_rank'] for node in self.graph.nodes}
        for i in range(num_iterations):
            nodes_rank = self.spread_nodes_rank(nodes_rank)
            print('Iteration done')
        for node in self.graph.nodes:
            self.graph.nodes[node]['rank'] = nodes_rank[node]
            node_degree = self.graph.degree(node, weight='weight')
            if node_degree > 0:
                self.graph.nodes[node]['rank'] /= node_degree
        return self.graph

    def spread_nodes_rank(self, nodes_rank):
        new_nodes_rank = {}
        for node in nodes_rank:
            new_trust = 0
            neighbors = self.graph.neighbors(node)
            for neighbor in neighbors:
                neighbor_degree = self.graph.degree(neighbor, weight='weight')
                edge_weight = self.graph[node][neighbor].get('weight', 1)
                if neighbor_degree > 0:
                    new_trust += nodes_rank[neighbor] * edge_weight / neighbor_degree
            new_nodes_rank[node] = new_trust
        return new_nodes_rank